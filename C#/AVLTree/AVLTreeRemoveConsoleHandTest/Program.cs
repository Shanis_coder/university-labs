﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AVLTree;

namespace AVLTreeRemoveConsoleHandTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new AVLTreeFactory<int>();
            AVLTreeNode<int> tree = factory.CreateTree(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, Comparer<int>.Default);

            PrintTree(tree, 0);

            for (int i = 0; i < 10; i++)
            {
                tree = factory.Remove(tree, i+1, Comparer<int>.Default);
                Console.WriteLine("**************************************************************");
                PrintTree(tree, 0);
            }

            Console.ReadLine();
        }

        private static void PrintTree(AVLTreeNode<int> tree, int p)
        {
            if (tree != null)
            {
                if (tree.LeftNode != null)
                    PrintTree(tree.LeftNode, p + 1);

                for (int i = 0; i < p; i++)
                    Console.Write("\t");
                Console.WriteLine(tree.Value.ToString());

                if (tree.RightNode != null)
                    PrintTree(tree.RightNode, p + 1);
            }
            else
                Console.WriteLine("null");
        }
    }
}
