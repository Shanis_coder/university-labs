﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AVLTree
{
    public class AVLTreeFactory<T>
    {

        AVLTreeNode<T> _treeRoot;

        /// <summary>
        /// Создание АВЛ-дерева
        /// </summary>
        public AVLTreeNode<T> CreateTree(IEnumerable<T> list, IComparer<T> comparer)
        {
            if (list == null || !list.Any())
                return null;

            _treeRoot = new AVLTreeNode<T>(list.ElementAt(0), null);

            foreach (var element in list.Skip(1))
            {
                InsertNewElementIntoTree(_treeRoot, element, comparer);
            }
            return _treeRoot;
        }
        /// <summary>
        ///  Рекурсивная вставка очередного узла в АВЛ-дерево
        /// </summary>
        private void InsertNewElementIntoTree(AVLTreeNode<T> treeNode, T element, IComparer<T> comparer)
        {
            int comparisonResult = comparer.Compare(element, treeNode.Value);

            if (comparisonResult == 0)
            {
                treeNode.Frequency++;
            }
            else if (comparisonResult > 0)
            {
                if (treeNode.RightNode == null)
                {
                    treeNode.RightNode = new AVLTreeNode<T>(element, treeNode);
                    treeNode.InvalidateHeight();
                }
                else
                {
                    InsertNewElementIntoTree(treeNode.RightNode, element, comparer);
                }
            }
            else
            {
                if (treeNode.LeftNode == null)
                {
                    treeNode.LeftNode = new AVLTreeNode<T>(element, treeNode);
                    treeNode.InvalidateHeight();
                }
                else
                {
                    InsertNewElementIntoTree(treeNode.LeftNode, element, comparer);
                }
            }
            // балансировка
            if (treeNode.ParentNode != null)
            {
                int balance = treeNode.ParentNode.BalanceFactor();
                if (Math.Abs(balance) > 1)
                {
                    BalanceAt(treeNode.ParentNode, balance);
                }
            }
        }
        /// <summary>
        /// Балансировка АВЛ-дерева
        /// </summary>
        public void BalanceAt(AVLTreeNode<T> node, int balance)
        {
            if (balance == 2) //правое поддерево тяжелее
            {
                int rightBalance = node.RightNode.BalanceFactor();
                if (rightBalance == 1 || rightBalance == 0)
                { // левое вращение
                    RotateLeft(node);
                }
                else if (rightBalance == -1)
                {  // двойной поворот
                    RotateRight(node.RightNode);
                    RotateLeft(node);
                }
            }
            else if (balance == -2) //левое поддерево тяжелее
            {
                int leftBalance = node.LeftNode.BalanceFactor();
                if (leftBalance == 1)
                {     // двойной поворот               
                    RotateLeft(node.LeftNode);
                    RotateRight(node);
                }
                else if (leftBalance == -1 || leftBalance == 0)
                {
                    //правое вращение
                    RotateRight(node);
                }
            }
        }
        /// <summary>
        /// Левый поворот в АВЛ-дереве
        /// </summary>
        public void RotateLeft(AVLTreeNode<T> root)
        {
            if (root == null)
                return;
            AVLTreeNode<T> pivot = root.RightNode;

            if (pivot == null)
                return;
            else
            {
                AVLTreeNode<T> rootParent = root.ParentNode; // отец узла с нарушенным балансом
                // текущий узел является левым ребенком?
                bool isLeftNode = (rootParent != null) && rootParent.LeftNode == root;
                // корень равен корню всего дерева?
                bool isTreeRoot = _treeRoot.Equals(root);
                //Поворот
                root.RightNode = pivot.LeftNode;
                pivot.LeftNode = root;

                //Корректировка родителей
                root.ParentNode = pivot;
                pivot.ParentNode = rootParent;

                if (root.RightNode != null)
                    root.RightNode.ParentNode = root;

                if (isLeftNode)
                    rootParent.LeftNode = pivot;
                else
                    if (rootParent != null)
                        rootParent.RightNode = pivot;

                if (isTreeRoot)
                    _treeRoot = pivot;
                root.InvalidateHeight();
            }
        }

        /// <summary>
        ///  Правый поворот в АВЛ-дереве
        /// </summary>
        public void RotateRight(AVLTreeNode<T> root)
        {
            if (root == null)
                return;
            AVLTreeNode<T> pivot = root.LeftNode;

            if (pivot == null)
                return;
            else
            {
                AVLTreeNode<T> rootParent = root.ParentNode; //отец узла с нарушенным балансом
                // текущий узел является левым ребенком?
                bool isLeftNode = (rootParent != null) && rootParent.LeftNode == root;
                // корень равен корню всего дерева?
                bool isTreeRoot = _treeRoot.Equals(root);
                //Поворот
                root.LeftNode = pivot.RightNode;
                pivot.RightNode = root;

                //Корректировка родителей
                root.ParentNode = pivot;
                pivot.ParentNode = rootParent;

                if (root.LeftNode != null)
                    root.LeftNode.ParentNode = root;

                if (isLeftNode)
                    rootParent.LeftNode = pivot;
                else
                    if (rootParent != null)
                        rootParent.RightNode = pivot;
                if (isTreeRoot)
                    _treeRoot = pivot;

                root.InvalidateHeight();
            }
        }

        public AVLTreeNode<T> Remove(AVLTreeNode<T> root, T element, IComparer<T> comparer)
        {
            if (root == null)
                return _treeRoot;

            if (comparer.Compare(root.Value, element) == 0)
            {
                RemoveElement(ref root);
                if (root != null)
                    BalanceAt(root, root.BalanceFactor());
            }
            else if (comparer.Compare(root.Value, element) < 0)
            {
                Remove(root.RightNode, element, comparer);
                BalanceAt(root, root.BalanceFactor());
            }
            else if (comparer.Compare(root.Value, element) > 0)
            {
                Remove(root.LeftNode, element, comparer);
                BalanceAt(root, root.BalanceFactor());
            }

            return _treeRoot;
        }

        private void RemoveElement(ref AVLTreeNode<T> element)
        {
            if (element.Frequency > 1)
            {
                element.Frequency--;
                return;
            }

            var isTreeRoot = element == _treeRoot;

            var change = FindChange(element);
            if (change == null)
            {
                if (element.ParentNode != null)
                {
                    if (element.ParentNode.LeftNode == element)
                        element.ParentNode.LeftNode = element.RightNode;
                    else
                        element.ParentNode.RightNode = element.RightNode;
                }
                if (element.RightNode != null)
                    element.RightNode.ParentNode = element.ParentNode;
                element = element.RightNode;
            }
            else
            {
                if (element.ParentNode != null)
                {
                    if (element.ParentNode.LeftNode == element)
                        element.ParentNode.LeftNode = change;
                    else
                        element.ParentNode.RightNode = change;
                }

                change.ParentNode = element.ParentNode;
                change.LeftNode = element.LeftNode;
                change.RightNode = element.RightNode;

                element = change;
            }
            if (element != null)
                BalanceAt(element, element.BalanceFactor());

            if (isTreeRoot)
                _treeRoot = element;
        }

        private AVLTreeNode<T> FindChange(AVLTreeNode<T> element)
        {
            if (element.LeftNode == null)
                return null;

            var result = element.LeftNode;

            if (result.RightNode != null)
                result = FindRight(result.RightNode);
            else
                ExtractRightNodeFromTree(result);

            BalanceAt(element, element.BalanceFactor());

            return result;
        }

        private AVLTreeNode<T> FindRight(AVLTreeNode<T> currentNode)
        {
            var result = currentNode;
            var parentNode = currentNode.ParentNode;

            if (currentNode.RightNode != null)
                result = FindRight(currentNode.RightNode);
            else
                ExtractLeftNodeFromTree(result);

            BalanceAt(parentNode, parentNode.BalanceFactor());
            return result;
        }

        private void ExtractLeftNodeFromTree(AVLTreeNode<T> result)
        {
            result.ParentNode.LeftNode = result.LeftNode;
            result.ParentNode = null;
            result.RightNode = null;
            result.LeftNode = null;
        }

        private void ExtractRightNodeFromTree(AVLTreeNode<T> node)
        {
            node.ParentNode.RightNode = node.LeftNode;
            node.LeftNode = null;
            node.RightNode = null;
            node.ParentNode = null;
        }
    }
}
