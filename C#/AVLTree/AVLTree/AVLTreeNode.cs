﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AVLTree
{
    public class AVLTreeNode<T>
    {
        private int? _height;

        public int Frequency { get; set; }

        public AVLTreeNode<T> LeftNode { get; set; }

        public AVLTreeNode<T> RightNode { get; set; }

        public AVLTreeNode<T> ParentNode { get; set; }

        public T Value { get; private set; }

        public AVLTreeNode(T value, AVLTreeNode<T> parent)
        {
            Frequency = 1;
            Value = value;
            ParentNode = parent;
        }

        public int Height
        {
            get
            {
                if (_height.HasValue)
                { return _height.Value; }

                int l = 0, r = 0;
                if (LeftNode != null)
                {
                    l = LeftNode.Height;
                }
                if (RightNode != null)
                {
                    r = RightNode.Height;
                }

                _height = 1 + Math.Max(r, l);
                return _height.Value;
            }
        }

        public void InvalidateHeight()
        {
            _height = null;

            if (ParentNode != null)
            {
                ParentNode.InvalidateHeight();
            }
        }

        public int BalanceFactor()
        {
            int l = 0, r = 0;
            if (LeftNode != null)
            {
                l = LeftNode.Height;
            }
            if (RightNode != null)
            {
                r = RightNode.Height;
            }
            return (r - l);
        }

    }
}

