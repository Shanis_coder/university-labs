﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AVLTree
{
    public class AVLTreeTraverse
    {
        public IEnumerable<AVLTreeNode<T>> Traverse<T>(AVLTreeNode <T> node)
        {
            var nodeWalkOrder = new List<AVLTreeNode<T>>();

            if (node != null)
            {
                nodeWalkOrder.Add(node);
                nodeWalkOrder.AddRange(Traverse(node.LeftNode));
                nodeWalkOrder.AddRange(Traverse(node.RightNode));
            }

            return nodeWalkOrder;
        }
    }
}
