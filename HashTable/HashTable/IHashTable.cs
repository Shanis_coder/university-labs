﻿using System.Collections.Generic;

namespace HashTable
{
    interface IHashTable<TKey, TValue>:IEnumerable<Pair<TKey, TValue>>
    {
        bool Delete(TKey key);
        void Push(TKey key, TValue value);
        TValue GetValueByKey(TKey key);
    }
}
