﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTable
{
    public class ChainHashTable<TKey, TValue> : HashTable<TKey, TValue>
    {
        private const uint DEFAULT_START_CAPACITY = 11;
        private List<Pair<TKey, TValue>>[] _table;
        private IPrimeGenerator _primeGenerator;
 
        public ChainHashTable() : this(new SimpleHashMaker<TKey>()) { }
        public ChainHashTable(HashMaker<TKey> hashMaker) : this(new EratosfenPrimeGenerator(), 
                                                                hashMaker, 
                                                                DEFAULT_START_CAPACITY) 
        { }

        private ChainHashTable(IPrimeGenerator primeGenerator, 
                               HashMaker<TKey> hashMaker, 
                               uint startCapacity)
        {
            _primeGenerator = primeGenerator;
            _capacity = startCapacity;
            _hashMaker = hashMaker;
            _hashMaker.SetDevider(_capacity);

            while (_primeGenerator.GetNext() < _capacity) ;

            _table = new List<Pair<TKey, TValue>>[_capacity];
        }

        public override bool Delete(TKey key)
        {
            if (key == null)
                throw new NullReferenceException();

            var hash = _hashMaker.GenerateHash(key);

            if (_table[hash] != null)
                foreach (var pair in _table[hash])
                    if (pair.Key.Equals(key))
                    {
                        Count--;
                        return _table[hash].Remove(pair);
                    }

            return false;
        }

        public override void Push(TKey key, TValue value)
        {
            if (key == null)
                throw new NullReferenceException();

            var hash = _hashMaker.GenerateHash(key);

            if (_table[hash] == null)
                _table[hash] = new List<Pair<TKey, TValue>>();
            else
                foreach (var pair in _table[hash])
                    if (pair.Key.Equals(key))
                        throw new ArgumentException("Key already exists");

            _table[hash].Add(new Pair<TKey, TValue>(key, value));

            Count++;

            if (NeedToRebuild())
                RebuildTable();
        }

        public override TValue GetValueByKey(TKey key)
        {
            if (key == null)
                throw new NullReferenceException();

            var hash = _hashMaker.GenerateHash(key);

            if (_table[hash] != null)
                foreach (var pair in _table[hash])
                    if (pair.Key.Equals(key))
                        return pair.Value;

            throw new KeyNotFoundException();
        }

        public override IEnumerator<Pair<TKey, TValue>> GetEnumerator()
        {
            var result = new List<Pair<TKey, TValue>>();

            foreach (var tableLine in _table)
                if (tableLine != null)
                    result.AddRange(tableLine);

            return result.GetEnumerator();
        }

        protected override void RebuildTable()
        {
            GenNewCapacity();

            var oldValues = this.GetEnumerator();
            _table = new List<Pair<TKey, TValue>>[_capacity];
            _hashMaker.SetDevider(_capacity);
            Count = 0;

            PushOldValuesIntoNewTable(oldValues);
        }

        private void GenNewCapacity()
        {
            while (_primeGenerator.GetNext() < 2 * _capacity) ;
            _capacity = _primeGenerator.GetNext();
        }

        private void PushOldValuesIntoNewTable(IEnumerator<Pair<TKey, TValue>> oldValues)
        {
            while (oldValues.MoveNext())
                Push(oldValues.Current.Key, oldValues.Current.Value);
        }
    }
}
