﻿using System;

namespace HashTable
{
    public abstract class HashTable<TKey, TValue>:IHashTable<TKey, TValue>
    {
        protected UInt32 _capacity;
        protected HashMaker<TKey> _hashMaker;

        public int Count { get; protected set; }

        public abstract bool Delete(TKey key);
        public abstract void Push(TKey key, TValue value);
        public abstract TValue GetValueByKey(TKey key);
        protected abstract void RebuildTable();
        
        public abstract System.Collections.Generic.IEnumerator<Pair<TKey, TValue>> GetEnumerator();

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() 
        {
            throw new NotImplementedException();
        }
        
        protected virtual bool NeedToRebuild()
        {
            return (double)Count / _capacity > 0.75;
        }
    }
}
