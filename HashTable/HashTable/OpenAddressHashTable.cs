﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTable
{
    public class OpenAddressHashTable<TKey, TValue>:HashTable<TKey, TValue>
    {
        private const uint DEFAULT_CAPACITY = 11;
        private Pair<TKey, TValue>[] _table;
        private IPositionGenerator<TKey> _positionGenrator;
        private IPrimeGenerator _primeGenerator;

        public OpenAddressHashTable() : this(new LinearPositionGenerator<TKey>()) { }

        public OpenAddressHashTable(IPositionGenerator<TKey> positionGnerator) :
            this(new SimpleHashMaker<TKey>(), positionGnerator, new EratosfenPrimeGenerator(), DEFAULT_CAPACITY) { }

        private OpenAddressHashTable(HashMaker<TKey> hashMaker, 
                                     IPositionGenerator<TKey> positionGenerator, 
                                     IPrimeGenerator primeGenerator, 
                                     uint capacity)
        {
            _capacity = capacity;
            _table = new Pair<TKey, TValue>[_capacity];
            _positionGenrator = positionGenerator;
            _primeGenerator = primeGenerator;
            _hashMaker = hashMaker;

            _positionGenrator.HashMaker = _hashMaker;
            _positionGenrator.Capacity = _capacity;
            _hashMaker.SetDevider(_capacity);
        }

        public override bool Delete(TKey key)
        {
            for (uint i = 0; i < _capacity; i++)
                if (_table[_positionGenrator.GenPosition(key, i)] != null &&
                    _table[_positionGenrator.GenPosition(key, i)].Key.Equals(key))
                {
                    _table[_positionGenrator.GenPosition(key, i)] = null;
                    Count--;
                    return true;
                }
            return false;
        }

        public override void Push(TKey key, TValue value)
        {
            for (uint i = 0; i < _capacity; i++)
                if (_table[_positionGenrator.GenPosition(key, i)] == null)
                {
                    _table[_positionGenrator.GenPosition(key, i)] = new Pair<TKey, TValue>(key, value);
                    break;
                }
                else if (_table[_positionGenrator.GenPosition(key, i)].Key.Equals(key))
                    throw new ArgumentException("Key already exists");

            Count++;

            if (NeedToRebuild())
                RebuildTable();
        }

        public override TValue GetValueByKey(TKey key)
        {
            for (uint i = 0; i < _capacity; i++)
                if (_table[_positionGenrator.GenPosition(key, i)] != null &&
                    _table[_positionGenrator.GenPosition(key, i)].Key.Equals(key))
                    return _table[_positionGenrator.GenPosition(key, i)].Value;

            throw new KeyNotFoundException();
        }

        public override IEnumerator<Pair<TKey, TValue>> GetEnumerator()
        {
            var result = new List<Pair<TKey, TValue>>();
            foreach (var tableItem in _table)
                if (tableItem != null)
                    result.Add(tableItem);

            return result.GetEnumerator();
        }

        protected override void RebuildTable()
        {
            while (_primeGenerator.GetNext() < 2 * _capacity) ;
            _capacity = _primeGenerator.GetNext();
            
            var oldValues = GetEnumerator();
            _hashMaker.SetDevider(_capacity);
            _positionGenrator.Capacity = _capacity;
            _table = new Pair<TKey, TValue>[_capacity];
            Count = 0;
            PushOldValuesIntoTable(oldValues);
        }

        private void PushOldValuesIntoTable(IEnumerator<Pair<TKey, TValue>> oldValues)
        {
            while (oldValues.MoveNext())
                Push(oldValues.Current.Key, oldValues.Current.Value);
        }
    }
}
