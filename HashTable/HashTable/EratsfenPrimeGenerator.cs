﻿using System;
using System.Collections.Generic;

namespace HashTable
{
    public class EratosfenPrimeGenerator:IPrimeGenerator
    {
        private List<uint> _knownPrimes;
        private uint _lastPrime;

        public EratosfenPrimeGenerator()
        {
            _knownPrimes = new List<uint>();
            _lastPrime = 1;
        }

        public uint GetNext()
        {
            while (_lastPrime < uint.MaxValue)
            {
                _lastPrime++;

                if (IsPrime())
                {
                    _knownPrimes.Add(_lastPrime);
                    return _lastPrime;
                }
            }
            throw new OverflowException("I can't make too large primes");
        }

        private bool IsPrime()
        {
            foreach (var prime in _knownPrimes)
                if ((_lastPrime % prime) == 0)
                    return false;
            
            return true;
        }
    }
}
