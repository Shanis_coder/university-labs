﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HashTable
{
    public interface IPositionGenerator<TKey>
    {
        HashMaker<TKey> HashMaker { get; set; }
        uint Capacity { get; set; }

        uint GenPosition(TKey key, uint iteration);
    }
}
