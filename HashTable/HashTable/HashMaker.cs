﻿using System;

namespace HashTable
{
    public abstract class HashMaker<T>
    {
        protected UInt32 _devider;

        abstract public void SetDevider(UInt32 devider);
        abstract public UInt32 GenerateHash(T key);

        public HashMaker(uint devider)
        {
            SetDevider(devider);
        }

        public HashMaker() : this(2) { }
    }
}
