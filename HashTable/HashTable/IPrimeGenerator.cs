﻿using System;

namespace HashTable
{
    public interface IPrimeGenerator
    {
        UInt32 GetNext();
    }
}
