﻿using System;

namespace HashTable
{
    public class SimpleHashMaker<TKey>:HashMaker<TKey>
    {
        public SimpleHashMaker(uint devider) : base(devider) { }
        public SimpleHashMaker() : base() { }

        public override void SetDevider(uint devider)
        {
            if (devider > (uint)1)
                _devider = devider;
            else
                throw new ArgumentException("devider must be > 1");
        }

        public override uint GenerateHash(TKey key)
        {
            return (uint) key.GetHashCode() % _devider;
        }
    }
}
