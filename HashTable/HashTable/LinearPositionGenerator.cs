﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HashTable
{
    class LinearPositionGenerator<TKey>:IPositionGenerator<TKey>
    {
        public HashMaker<TKey> HashMaker { get; set; }
        public uint Capacity { get; set; }

        public uint GenPosition(TKey key, uint iteration)
        {
            return (HashMaker.GenerateHash(key) + iteration) % Capacity;
        }
    }
}
