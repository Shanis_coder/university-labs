﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HashTable;

namespace UnitTestHashTable
{
    [TestClass]
    public class SimpleHashMakerTest
    {
        [TestMethod]
        public void GetCorrectHashForIntWithDefaultSettings()
        {
            var hashMaker = new SimpleHashMaker<int>();

            var expectedValue = (uint)((2).GetHashCode() % 2);
            var actualValue = hashMaker.GenerateHash(2);

            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void GetCorrectHashForIntWithSettingsSetInConstructor()
        {
            var hashMaker = new SimpleHashMaker<int>(7);

            var expectedValue = (uint)((2).GetHashCode() % 7);
            var actualValue = hashMaker.GenerateHash(2);

            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void GetCorrectHashForIntWithSettingsSetInSetDevider()
        {
            var hashMaker = new SimpleHashMaker<int>();
            hashMaker.SetDevider(7);

            var expectedValue = (uint)((2).GetHashCode() % 7);
            var actualValue = hashMaker.GenerateHash(2);

            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDevider1()
        {
            var hashMaker = new SimpleHashMaker<int>();
            hashMaker.SetDevider(1);            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenDevider0()
        {
            var hashMaker = new SimpleHashMaker<int>();
            hashMaker.SetDevider(0);
        }
    }
}
