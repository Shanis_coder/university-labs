﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HashTable;

namespace UnitTestHashTable
{
    [TestClass]
    public class ChainHashTableTests
    {
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void ExceptionWhenTryGetValueFromEmptyTable()
        {
            var hashTable = new ChainHashTable<int, int>();
            hashTable.GetValueByKey(0);
        }

        [TestMethod]
        public void ReturnFalseWhenTryDeleteKeyFromEmptyTable()
        {
            var hashTable = new ChainHashTable<int, int>();

            Assert.AreEqual(false, hashTable.Delete(0));
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void NullReferenceExceptionWhenKeyForPushIsNull()
        {
            var hashTable = new ChainHashTable<int?, int>();
            hashTable.Push(null, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void NullReferenceExceptionWhenKeyForGetIsNull()
        {
            var hashTable = new ChainHashTable<int?, int>();
            hashTable.GetValueByKey(null);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void NullReferenceExceptionWhenKeyForDeleteIsNull()
        {
            var hashTable = new ChainHashTable<int?, int>();
            hashTable.Delete(null);
        }

        [TestMethod]
        public void PushPairK0V2GetValue2ByKey0()
        {
            var hashTable = new ChainHashTable<int, int>();
            hashTable.Push(0, 2);

            Assert.AreEqual(2, hashTable.GetValueByKey(0));
        }

        [TestMethod]
        public void ReturnTrueWhenDeleteExistingKeyFromTable()
        {
            var hashTable = new ChainHashTable<int, int>();
            hashTable.Push(0, 2);

            Assert.AreEqual(true, hashTable.Delete(0));
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void ExceptionWhenTryGetNotExistsKeyFromTable()
        {
            var hashTable = new ChainHashTable<int, int>();
            hashTable.Push(0, 2);

            hashTable.GetValueByKey(1);
        }

        [TestMethod]
        public void ReturnFalseWhenTryDeleteNotExistsKeyFromTable()
        {
            var hashTable = new ChainHashTable<int, int>();

            Assert.AreEqual(false, hashTable.Delete(0));
        }

        [TestMethod]
        public void CorrectlyWorkingWithBigRangeOfKeys()
        {
            var hashTable = new ChainHashTable<int, int>();

            for (int i = 0; i < 1000; i++)
                hashTable.Push(i, i + 2);

            for (int i = 0; i < 1000; i++)
                Assert.AreEqual(i + 2, hashTable.GetValueByKey(i));
        }

        [TestMethod]
        public void Count0InEmptyTable()
        {
            var hashTable = new ChainHashTable<int, int>();

            Assert.AreEqual(0, hashTable.Count);
        }

        [TestMethod]
        public void Count1AfterAdding1Element()
        {
            var hashTable = new ChainHashTable<int, int>();
            hashTable.Push(0, 2);

            Assert.AreEqual(1, hashTable.Count);
        }

        [TestMethod]
        public void Count0AfterAddingAndRemoving1Element()
        {
            var hashTable = new ChainHashTable<int, int>();
            hashTable.Push(0, 2);
            hashTable.Delete(0);

            Assert.AreEqual(0, hashTable.Count);
        }

        [TestMethod]
        public void Count1AfterAdding2AndRemoving1Element()
        {
            var hashTable = new ChainHashTable<int, int>();
            hashTable.Push(0, 2);
            hashTable.Push(1, 2);
            hashTable.Delete(0);

            Assert.AreEqual(1, hashTable.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptionWhenTryAddExistingKey()
        {
            var hashTable = new ChainHashTable<int, int>();
            hashTable.Push(0, 2);
            hashTable.Push(0, 3);
        }

        [TestMethod]
        public void CountNotChangeWhenTryAddExistingKey()
        {
            var hashTable = new ChainHashTable<int, int>();
            try
            {
                hashTable.Push(0, 2);
                hashTable.Push(0, 3);
            }
            catch
            {
            }

            Assert.AreEqual(1, hashTable.Count);
        }
    }
}
