﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HashTable;

namespace UnitTestHashTable
{
    [TestClass]
    public class OAHTTests
    {
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void ExceptWhenTryGetValueFromEmptyTable()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            oaht.GetValueByKey(0);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void ExceptWhenTryGetValueNotExiostsKey()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            oaht.Push(0, 2);
            oaht.Push(2, 0);
            oaht.GetValueByKey(1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ExceptWhenTryPushExistingKey()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            oaht.Push(0, 2);
            oaht.Push(0, 0);
        }

        [TestMethod]
        public void ReturnCorrectValueFor1ElementTable()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            oaht.Push(0, 2);
            Assert.AreEqual(2, oaht.GetValueByKey(0));
        }

        [TestMethod]
        public void Count1For1ElementTable()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            oaht.Push(0, 2);
            Assert.AreEqual(1, oaht.Count);
        }

        [TestMethod]
        public void Count2For2ElementTable()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            oaht.Push(0, 2);
            oaht.Push(1, 2);
            Assert.AreEqual(2, oaht.Count);
        }

        [TestMethod]
        public void NotAddCountWhenTryPushExistingKey()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            try
            {
                oaht.Push(0, 2);
                oaht.Push(0, 1);
            }
            catch
            { }
            
            Assert.AreEqual(1, oaht.Count);
        }

        [TestMethod]
        public void CorrectlyCountForBigRange()
        {
            var oaht = new OpenAddressHashTable<int, int>();

            for (int i = 0; i < 100; i++)
                oaht.Push(i, i + 2);

            Assert.AreEqual(100, oaht.Count);
        }

        [TestMethod]
        public void GetCorrectlyValuesForBigRange()
        {
            var oaht = new OpenAddressHashTable<int, int>();

            for (int i = 0; i < 100; i++)
                oaht.Push(i, i + 2);

            for (int i = 0; i < 100; i++)
                Assert.AreEqual(i + 2, oaht.GetValueByKey(i));
        }

        [TestMethod]
        public void ReturnTrueWhenRemoveExistingItem()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            oaht.Push(0, 2);
            Assert.AreEqual(true, oaht.Delete(0));
        }

        [TestMethod]
        public void ReturnFalseWhenRemoveNotExistingItem()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            oaht.Push(0, 2);
            Assert.AreEqual(false, oaht.Delete(1));
        }

        [TestMethod]
        public void ReturnFalseWhenTryRemoveFromEmptyTable()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            
            Assert.AreEqual(false, oaht.Delete(0));
        }

        [TestMethod]
        public void DecCountAfterRemoving()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            oaht.Push(0, 2);
            oaht.Delete(0);

            Assert.AreEqual(0, oaht.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void ExceptionWhenTryGetDeletedItem()
        {
            var oaht = new OpenAddressHashTable<int, int>();
            
            oaht.Push(0, 2);
            oaht.Push(1, 0);
            oaht.Delete(1);

            oaht.GetValueByKey(1);
        }
    }
}
