﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HashTable;

namespace UnitTestHashTable
{
    [TestClass]
    public class EratosfenPrimeGeneratorTests
    {
        [TestMethod]
        public void FirstPrimeIs2()
        {
            var primeGenerator = new EratosfenPrimeGenerator();
            var firstPrime = primeGenerator.GetNext();

            Assert.AreEqual((uint)2, firstPrime);
        }

        [TestMethod]
        public void GenerateCorrectPrimeSeqence()
        {
            var primeGenerator = new EratosfenPrimeGenerator();
            var expectedSequence = new uint[] { 2, 3, 5, 7, 11 };
            
            var actualSeqence = new uint[5];
            for (int i = 0; i < 5; i++)
                actualSeqence[i] = primeGenerator.GetNext();

            CollectionAssert.AreEqual(expectedSequence, actualSeqence);
        }
    }
}
