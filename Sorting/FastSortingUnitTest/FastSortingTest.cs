﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ArraySorting;

namespace FastSortingUnitTest
{
    [TestClass]
    public class FastSortingTest
    {
        [TestMethod]
        public void CorrectResultOnSortedArray()
        {
            var fastSorting = new FastSorting<int>();

            var array = new int[5] { 1, 2, 3, 4, 5 };
            var expected = new int[5] { 1, 2, 3, 4, 5 };

            fastSorting.Sort(array, Comparer<int>.Default);

            CollectionAssert.AreEqual(expected, array);
        }

        [TestMethod]
        public void CorrectResultOnReversedArray()
        {
            var fastSorting = new FastSorting<int>();

            var array = new int[5] { 5, 4, 3, 2, 1 };
            var expected = new int[5] { 1, 2, 3, 4, 5 };

            fastSorting.Sort(array, Comparer<int>.Default);

            CollectionAssert.AreEqual(expected, array);
        }

        [TestMethod]
        public void CorrectResultOnArrayOfEqalsElements()
        {
            var fastSorting = new FastSorting<int>();

            var array = new int[5] { 0, 0, 0, 0, 0 };
            var expected = new int[5] { 0, 0, 0, 0, 0 };

            fastSorting.Sort(array, Comparer<int>.Default);

            CollectionAssert.AreEqual(expected, array);
        }
    }
}
