﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ArraySorting;

namespace PiramidSortingUnitTests
{
    [TestClass]
    public class PiramidSortingTests
    {
        [TestMethod]
        public void NotExceptionOnEmptyArray()
        {
            var piramidSorting = new PiramidSorting<int>();
            
            var array = new int[0];
            var sortArray = new int[0];
            piramidSorting.Sort(sortArray, Comparer<int>.Default);

            CollectionAssert.AreEqual(array, sortArray);
        }

        [TestMethod]
        public void NotExceptionOn1ElementArray()
        {
            var piramidSorting = new PiramidSorting<int>();

            var array = new int[1]{1};
            var sortArray = new int[1]{1};
            piramidSorting.Sort(sortArray, Comparer<int>.Default);

            CollectionAssert.AreEqual(array, sortArray);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void NullReferenceExceptionWhenArrayNull()
        {
            var piramidSorting = new PiramidSorting<int>();

            piramidSorting.Sort(null, Comparer<int>.Default);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void NullReferenceExceptionWhenComparerNull()
        {
            var piramidSorting = new PiramidSorting<int>();

            piramidSorting.Sort(new int[1]{1}, null);
        }

        [TestMethod]
        public void CorrectResultOnSortedArray()
        {
            var piramidSorting = new PiramidSorting<int>();

            var array = new int[5] { 1, 2, 3, 4, 5 };
            var sortArray = new int[5] { 1, 2, 3, 4, 5 };
            piramidSorting.Sort(sortArray, Comparer<int>.Default);

            CollectionAssert.AreEqual(array, sortArray);
        }

        [TestMethod]
        public void CorrectResultOnReverseArray()
        {
            var piramidSorting = new PiramidSorting<int>();

            var array = new int[5] { 1, 2, 3, 4, 5 };
            var sortArray = new int[5] { 5, 4, 3, 2, 1 };
            piramidSorting.Sort(sortArray, Comparer<int>.Default);

            CollectionAssert.AreEqual(array, sortArray);
        }
    }
}
