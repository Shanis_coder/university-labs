﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ArraySorting;

namespace PiramidSortingUnitTests
{
    [TestClass]
    public class HeapBuilderTests
    {
        [TestMethod]
        public void Build5ElementHeapFromSortedArray()
        {
            var builder = new HeapBuilder<int>();

            var array = new int[5] {1, 2, 3, 4, 5 };
            var expected = new int[5] { 5, 4, 3, 1, 2 };

            builder.BuildHeap(array, 5, Comparer<int>.Default);

            CollectionAssert.AreEqual(expected, array);
        }

        [TestMethod]
        public void Restore4ElementHeapFrom5ElementsArray()
        {
            var builder = new HeapBuilder<int>();

            var array = new int[5] { 2, 4, 3, 1, 5 };
            var expected = new int[5] { 4, 2, 3, 1, 5 };

            builder.RestoreHeapFrom(0, array, 4, Comparer<int>.Default);

            CollectionAssert.AreEqual(expected, array);
        }

        [TestMethod]
        public void Restore3ElementHeapFrom5ElementsArray()
        {
            var builder = new HeapBuilder<int>();

            var array = new int[5] { 1, 2, 3, 4, 5 };
            var expected = new int[5] { 3, 2, 1, 4, 5 };

            builder.RestoreHeapFrom(0, array, 3, Comparer<int>.Default);

            CollectionAssert.AreEqual(expected, array);
        }

        [TestMethod]
        public void Restore2ElementHeapFrom5ElementsArray()
        {
            var builder = new HeapBuilder<int>();

            var array = new int[5] { 1, 2, 3, 4, 5 };
            var expected = new int[5] { 2, 1, 3, 4, 5 };

            builder.RestoreHeapFrom(0, array, 2, Comparer<int>.Default);

            CollectionAssert.AreEqual(expected, array);
        }

        [TestMethod]
        public void Restore1ElementHeapFrom5ElementsArray()
        {
            var builder = new HeapBuilder<int>();

            var array = new int[5] { 1, 2, 3, 4, 5 };
            var expected = new int[5] { 1, 2, 3, 4, 5 };

            builder.RestoreHeapFrom(0, array, 1, Comparer<int>.Default);

            CollectionAssert.AreEqual(expected, array);
        }
    }
}
