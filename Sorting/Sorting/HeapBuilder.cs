﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.CompilerServices;
[assembly: InternalsVisibleTo("PiramidSortingUnitTests")]

namespace ArraySorting
{
    class HeapBuilder<T>
    {
        public void BuildHeap(T[] array, int size, IComparer<T> comparer)
        {
            for (int i = (size - 1) / 2; i >= 0; i--)
                RestoreHeapFrom(i, array, size, comparer);
        }

        public void RestoreHeapFrom(int index, T[] array, int size, IComparer<T> comparer)
        {
            if (index <= (size - 1) / 2)
            {
                if ((index * 2 + 1 < size) && (index * 2 + 2 < size))
                {
                    if (comparer.Compare(array[index], Max(array[index * 2 + 1], array[index * 2 + 2], comparer)) < 0)
                        RestoreHeapFrom(ChangeElements(index, array, comparer),
                                           array, size, comparer);
                }
                else
                    if ((index * 2 + 1 < size) && comparer.Compare(array[index], array[index * 2 + 1]) < 0)
                    {
                        ChangeElements(index, index * 2 + 1, array);
                        RestoreHeapFrom(index * 2 + 1, array, size, comparer);
                    }
            }
        }

        private T Max(T t1, T t2, IComparer<T> comparer)
        {
            return comparer.Compare(t1, t2) > 0 ? t1 : t2;
        }

        private int ChangeElements(int index, T[] array, IComparer<T> comparer)
        {
            var indexOfMax = GetIndexOfMax(index * 2 + 1, index * 2 + 2, array, comparer);

            ChangeElements(index, indexOfMax, array);

            return indexOfMax;
        }

        private static void ChangeElements(int index1, int index2, T[] array)
        {
            var buf = array[index1];
            array[index1] = array[index2];
            array[index2] = buf;
        }

        private int GetIndexOfMax(int ind1, int ind2, T[] array, IComparer<T> comparer)
        {
            return comparer.Compare(array[ind1], array[ind2]) > 0 ? ind1 : ind2;
        }

    }
}
