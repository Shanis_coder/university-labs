﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraySorting
{
    public class PiramidSorting<T>:IArraySorting<T>
    {
        private HeapBuilder<T> _heapBuilder;

        public PiramidSorting()
        {
            _heapBuilder = new HeapBuilder<T>();
        }

        public void Sort(T[] array, IComparer<T> comparer)
        {
            if (array == null || comparer == null)
                throw new NullReferenceException();

            var notSortedElementsCount = array.Length;
            _heapBuilder.BuildHeap(array, notSortedElementsCount, comparer);

            for (; notSortedElementsCount > 0; )
            {
                ChangeFirstLast(array, notSortedElementsCount);
                _heapBuilder.RestoreHeapFrom(0, array, --notSortedElementsCount, comparer);
            }
        }

        private void ChangeFirstLast(T[] array, int notSortedElementsCount)
        {
            var buf = array[0];
            array[0] = array[notSortedElementsCount - 1];
            array[notSortedElementsCount - 1] = buf;
        }
    }
}
