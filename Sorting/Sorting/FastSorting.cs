﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraySorting
{
    public class FastSorting<T>:IArraySorting<T>
    {

        public void Sort(T[] array, IComparer<T> comparer)
        {
            Sort(array, 0, array.Length - 1, comparer);
        }

        private void Sort(T[] array, int startIndex, int endIndex, IComparer<T> comparer)
        {
            var baseElement = startIndex + (endIndex - startIndex) / 2;
            var left = startIndex;
            var right = endIndex;

            if (baseElement == startIndex)
                return;

            while (left < right)
            {
                MoveLeft(array, ref left, baseElement, comparer);
                MoveRight(array, ref right, baseElement, comparer);

                if (left != right)
                    ChangeElements(array, left, right);

                if (left == baseElement)
                    baseElement = right;
                if (right == baseElement)
                    baseElement = left;

                left++; right--;
            }

            Sort(array, startIndex, baseElement, comparer);
            Sort(array, baseElement, endIndex, comparer);
        }

        private void ChangeElements(T[] array, int left, int right)
        {
            var buf = array[left];
            array[left] = array[right];
            array[right] = buf;
        }

        private void MoveRight(T[] array, ref int right, int baseElement, IComparer<T> comparer)
        {
            for (; (right > baseElement) && (comparer.Compare(array[right], array[baseElement]) > 0); right--) ;
        }

        private void MoveLeft(T[] array, ref int left, int baseElement, IComparer<T> comparer)
        {
            for (; (left < baseElement) && (comparer.Compare(array[left], array[baseElement]) < 0); left++);
        }
    }
}
